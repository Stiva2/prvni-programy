public class Mnohoclen {
  private awh.IntList cleny;
  public Mnohoclen(awh.IntList obrat) {
    obrat.reverse();
    cleny = obrat;
  }
//scitani
  public Mnohoclen plus(Mnohoclen b) {

    while (cleny.size() != b.size()) {
      if (cleny.size() < b.size()){
        cleny.add(0);
      } else {
        b.cleny.add(0);
      }
    }
    awh.IntList vysledek = new awh.IntList();
    for (int i = 0; i < cleny.size(); i++) {
      int soucet = cleny.get(i) + b.get(i);
      vysledek.add(soucet);
    }
    vysledek.reverse();
    return new Mnohoclen(vysledek);
  }

//odecitani
  public Mnohoclen minus(Mnohoclen b) {

    while (cleny.size() != b.size()) {
      if (cleny.size() < b.size()){
        cleny.add(0);
      } else {
        b.cleny.add(0);
      }
    }

    awh.IntList vysledek = new awh.IntList();
    for (int i = 0; i < cleny.size(); i++) {
      int rozdil = cleny.get(i) - b.get(i);
      vysledek.add(rozdil);
    }
    vysledek.reverse();
    return new Mnohoclen(vysledek);
  }
  
  public int size() {
    return cleny.size();
  }

  public int get(int index) {
    return cleny.get(index);
  }
  public String toString() {
    StringBuilder vysledek = new StringBuilder();
    for (int i = cleny.size() - 1; i >= 0; i--) {
      if (i == 0) {
        vysledek.append(cleny.get(i));
      } else if (i == 1) {
        vysledek.append(cleny.get(i));
        vysledek.append("x");
        vysledek.append(" + ");
      } else {
        vysledek.append(cleny.get(i));
        vysledek.append("x^");
        vysledek.append(i);
        vysledek.append(" + ");
      }
    }
    return vysledek.toString();
  }
  public int getDegree() {
    return cleny.size() - 1;
  }
  public int getCoef(int stupen) {
    return cleny.get(stupen);
  }
}
