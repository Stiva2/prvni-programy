public class KomCislo {
  public static void main(String[] args) {
    double n = Double.parseDouble(args[0]);
    double k = Double.parseDouble(args[1]);
      if (n >= k && k>= 0) {
        System.out.println((faktorial(n)) / ((faktorial(k)) * (faktorial(n - k))));
    } else {
        System.out.println(0);
    }
  }
  public static double faktorial(double x){
    double v = 1.0;
    for (double i = x; i > 0; i--) {
      v *= i;
    }
  return v;
  }
}
  

        