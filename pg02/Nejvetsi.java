public class Nejvetsi{
  public static void main(String[] args) {
    int nejv = 0;
    java.util.Scanner sc = new java.util.Scanner(System.in);
    while (sc.hasNextInt()) {
      int a = sc.nextInt();
      if (a <= nejv) {
        continue;
      }
      else {
        nejv = a;
      }
    }
    System.out.println(nejv);
  }
}
