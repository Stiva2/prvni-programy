public class Prvocislo{
  public static void main(String[] args) {
    int a = Integer.parseInt(args[0]);
    int prv = 1;
    for (int y = (a - 1); y > 1; y--) {
        if ((a % y) == 0) {
          prv = 0;
        } else {
          continue;
        }
      }
    
    boolean jePrvocislo = (prv == 1);
    if (jePrvocislo) {
      System.out.println ("Je prvocislo");
    } 
    if (!jePrvocislo) {
      System.out.println ("Neni prvocislo");
    }
  }
}