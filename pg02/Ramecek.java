public class Ramecek {
	public static void main(String[] args) {
		int sirka = Integer.parseInt(args[0]);
		int vyska = Integer.parseInt(args[1]);
		int okraj = Integer.parseInt(args[2]);
		for(int y = 0; y < vyska + 2 * okraj; y++) {
			for(int x = 0; x < sirka + 2 * okraj; x++) {
				if(x < okraj
						|| x >= okraj + sirka
						|| y < okraj
						|| y >= okraj + vyska) {
					System.out.print("X");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}
