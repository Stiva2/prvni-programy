public class Sediva {
	public static void main(String[] args) {
		awh.Image obr = awh.Image.loadFromFile(args[0]);
		for(int x = 0; x < obr.getWidth(); x++) {
			for(int y = 0; y < obr.getHeight(); y++) {
				awh.Color barva = obr.getPixel(x, y);
				int blue = barva.getBlue();
				int green = barva.getGreen();
				int red = barva.getRed();
				int grey = (int)(0.700 * red + 0.100 * green + 0.200 * blue);
				awh.Color newBarva = new awh.Color(grey, grey, grey);
				obr.setPixel(x, y, newBarva);
			}
		}
		obr.saveToFile(args[1]);
	}
}