public class Trojuhelnik {
	public static void main(String[] args) {
		int pocet = Integer.parseInt(args[0]);
		for(int a = 0; a < pocet; a++){
			for(int b = 0; b < pocet; b++){
				if(b <= a){
					System.out.print("X");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}
