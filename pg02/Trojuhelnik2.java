public class Trojuhelnik2 {
	public static void main(String[] args) {
		int pocet = Integer.parseInt(args[0]);
		for(int a = 0; a < pocet; a++){
			for(int b = 0; b < pocet; b++){
				if((b + 1) < (pocet - a)){
					System.out.print(" ");
				} else {
					System.out.print("X");
				}
			}
			System.out.println();
		}
	}
}