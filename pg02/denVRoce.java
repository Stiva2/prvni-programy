public class denVRoceCoFunguje {
  public static void main(String[] args) {
    int rok = Integer.parseInt(args[0]);
    int mesic = Integer.parseInt(args[1]);
    int den = Integer.parseInt(args[2]);
    int pocet = 0;
    pocet += dnuVMesici(mesic);
    pocet += den;
    if (jePrestupny(rok) && (mesic > 2)) {
      pocet -= 1;
    } else {
      if (mesic <= 2) {
        pocet -= 0;
      } else {
        pocet -= 2;
      }
    } 
    if (datumExistuje (den, mesic)){
      System.out.printf("pocet = %d\n", pocet);
    } else {
      System.out.print("Datum neexistuje.");
    }
  }
  public static boolean jePrestupny(int rok) {
    return ((rok % 4) == 0 && (rok % 100 != 0)) || ((rok % 400) == 0);
  }
  public static int dnuVMesici(int mesic){
    int pocet = 0;
    if (mesic <= 8) {
      if ((mesic % 2) == 0) {
        pocet += ((mesic / 2) * 31);
        pocet += (((mesic / 2) - 1) * 30);
      } else {
        pocet += ((mesic / 2) * 31);
        pocet += ((mesic / 2) * 30);
      }
    } else {
      pocet += 214;
      if ((mesic % 2) == 0) {
        pocet += (((mesic - 8) / 2) * 30);
        pocet += (((mesic - 8) / 2) * 31);
      } else {
        pocet += (((mesic - 8) / 2) * 30);
        pocet += ((((mesic - 8) / 2) + 1) * 31);
      }
    }
  return pocet;
  }
  public static boolean datumExistuje(int den, int mesic, int rok) {
    if (jePrestupny(rok)){
      return (den < 32) && (mesic == 1)
       || (den < 32) && (mesic == 3)
       || (den < 32) && (mesic == 5)
       || (den < 32) && (mesic == 7)
       || (den < 32) && (mesic == 8)
       || (den < 32) && (mesic == 10)
       || (den < 32) && (mesic == 12)
       || (den < 31) && (mesic == 4)
       || (den < 31) && (mesic == 6)
       || (den < 31) && (mesic == 9)
       || (den < 31) && (mesic == 11)
       || (den < 30) && (mesic == 2);  
    } else {
      return (den < 32) && (mesic == 1)
       || (den < 32) && (mesic == 3)
       || (den < 32) && (mesic == 5)
       || (den < 32) && (mesic == 7)
       || (den < 32) && (mesic == 8)
       || (den < 32) && (mesic == 10)
       || (den < 32) && (mesic == 12)
       || (den < 31) && (mesic == 4)
       || (den < 31) && (mesic == 6)
       || (den < 31) && (mesic == 9)
       || (den < 31) && (mesic == 11)
       || (den < 29) && (mesic == 2);  
    }
  }
}       